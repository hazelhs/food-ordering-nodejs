const {Menu, validate} = require('../models/menu');
const express = require('express');
const router =  express.Router();
const Joi = require('joi');

//To prevent errors from Cross Origin Resource Sharing, we will set 
//our headers to allow CORS with middleware like so:
router.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Credentials", "true");
    res.setHeader("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT,DELETE");
    res.setHeader("Access-Control-Allow-Headers", 
     "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
   //and remove cacheing so we get the most recent comments
    res.setHeader("Cache-Control", "no-cache");
    next();
});


router.get("/", async(req, res) => {

    if(req.query.category) {
        console.log("filter list with category ", req.query.category);
        const menus = await Menu.find({category: req.query.category});
        res.send(menus);
    }
    else if (req.query.searchByName) {
        console.log("query by menu name ", req.query.searchByName);
        const menus = await Menu
                        .find({name: {$regex: req.query.searchByName, $options:'i'}});
        res.send(menus);
    }

    else {
        console.log("getting all menus ....");
        console.log('query ', req.query);

        const pageNum = parseInt(req.query.page);
        const pageSize = parseInt(req.query.limit);

        console.log('pageNum ', pageNum);
        console.log('pageSize ', pageSize);

        const menus = await Menu
                .find()
                .skip((pageNum - 1) * pageSize)
                .limit(pageSize)
                .sort("-category");
        res.send(menus);
    }
});



router.get("/:id", async(req, res) => {
    let menu = await Menu.findById(req.params.id);
    if(!menu) return res.status(404).send("Menu with given ID not found");
    res.send(menu);
});

router.post('/', async(req, res) => {
    console.log("creating new menu ... " , req.body);
    let menu = new Menu({
        name: req.body.name,
        category: req.body.category,
        imageUrl: req.body.imageUrl,
        price: req.body.price
    });
    const newMenu = await menu.save();
    console.log("created new menu");
    res.status(200).send(newMenu);
});

router.put('/:id', async(req, res) => {
    console.log("updating menu ..." + req.params.id);
    const menu = await Menu.findById(req.params.id);
    if(!menu) return res.status(404).send("Menu with given ID not found");

    console.log("menu details to update... ", req.body)
    menu.name = req.body.name;
    menu.imageUrl = req.body.imageUrl;
    menu.price = req.body.price;
    menu.category = req.body.category;

    const result = await menu.save();
    res.status(200).send(result);
});

router.delete('/:id', async (req, res) => {
    
    console.log("deleting menu ..." + req.params.id);
    const menu = await Menu.findByIdAndDelete(req.params.id);
    if (!menu) return res.status(404).send("The menu with the given ID was not found.");

    const menus = await Menu.find();
    res.status(200).send(menus);
});


module.exports = router;
